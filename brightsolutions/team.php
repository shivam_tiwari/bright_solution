			<!DOCTYPE html>
<html lang="en">
  	<head>
  		<meta name="google-site-verification" content="g14srv2TqdDLsOHp3c6TysVix5wVzLPTjtK648gnWK0" />
  		<!-- <base href="/" /> -->
	    <meta charset="utf-8">
	    <title>Welcome to Bright Solutions</title>
		
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="bright solutions">

	    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	    <link rel="stylesheet" href="assets/css/fonts.css">
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
		<script type="text/javascript" src="assets/js/minified.js.php"> </script>
		<link id="qstyle" rel="stylesheet" href="assets/css/themes/style-4.css">
		<link rel="stylesheet" href="assets/css/plugins/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/plugins/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="assets/css/plugins/colorBox/colorbox.css">
		<link rel="stylesheet" type="text/css" href="assets/js/plugins/rs-slider/rs-plugin/css/settings.css" media="screen" />
		<link rel="stylesheet" href="assets/css/mycss.css">

	</head>
	<body>

		<?php include ("views/modules/main-navigation.html"); ?>
    		<div class="page-container">
		    
				<?php include ("views/static/team.html");?>
				<?php include ("views/modules/footer.html");?>
				
			</div>


	 	<script type="text/javascript" src="assets/js/footer_minified.js.php"></script>
	    <!-- <script src="assets/js/jquery.min.js"></script> -->
	  <!--   <script src="assets/js/bootstrap.min.js"></script> -->
		<!-- <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script> -->
		<!-- <script src="assets/js/plugins/pace/pace.min.js"></script> -->
		<!-- <script type="text/javascript" src="assets/js/plugins/rs-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>    -->
		<!-- <script type="text/javascript" src="assets/js/plugins/rs-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> -->
		<script type="text/javascript" src="assets/js/plugins/owl-carousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="assets/js/plugins/colorBox/jquery.colorbox-min.js"></script>
		<script src="assets/js/main.js"></script>
		<script type="text/javascript">
		// init variables require for all front pages
			Apps.initNavTopBar();
		// end
		
		//Rv Slider
		jQuery('.tp-banner').revolution( {
			delay:9000,
			startwidth:1150,
			startheight:450,
			hideThumbs:10,
			

	        thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
	        thumbHeight:50,
	        thumbAmount:5,

	        navigationType:"bullet",                // bullet, thumb, none
	        navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none

	        navigationStyle:"square",                // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


	        navigationHAlign:"center",              // Vertical Align top,center,bottom
	        navigationVAlign:"bottom",              // Horizontal Align left,center,right
	        navigationHOffset:0,
	        navigationVOffset:20,

	        soloArrowLeftHalign:"left",
	        soloArrowLeftValign:"center",
	        soloArrowLeftHOffset:20,
	        soloArrowLeftVOffset:0,

	        soloArrowRightHalign:"right",
	        soloArrowRightValign:"center",
	        soloArrowRightHOffset:20,
	        soloArrowRightVOffset:0,

	        touchenabled:"on",                      // Enable Swipe Function : on/off
	        onHoverStop:"on",                       // Stop Banner Timet at Hover on Slide on/off

	        stopAtSlide:-1,
	        stopAfterLoops:-1,

	        hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
			hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
			hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value

	        shadow:0,                               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
	        fullWidth:"on"               
		});
		
		//colorbox // for recent work slider
		jQuery(function($) {
			var colorbox_params = {
			reposition:true,
			scalePhotos:true,
			scrolling:false,
			previous:'<i class="fa fa-angle-left"></i>',
			next:'<i class="fa fa-angle-left"></i>',
			close:'<i class="fa fa-times text-danger"></i>',
			current:'{current} of {total}',
			maxWidth:'100%',
			maxHeight:'100%',
			onOpen:function(){
			document.body.style.overflow = 'hidden';
			},
			onClosed:function(){
			document.body.style.overflow = 'auto';
			},
			onComplete:function(){
				$.colorbox.resize();
			}
		};

		$('.slide-item [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='fa fa-spinner fa-spin'></i>");//let's add a custom loading icon
		
		
		$(".owl-carousel-1").owlCarousel({  // for recent work slider
                pagination: true,
                navigation: false
          });

		})
	</script>
	    
	</body>
</html>