var app = angular.module('app', ['ui.router', 'static.app', 'ui.validate', 'ngMessages']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider' , function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('home', {
        'url': "/",
        views: {
            'topnav':{
                templateUrl: "views/modules/main-navigation.html"
            },
            'main':{
                templateUrl: "views/static/home.html",
                controller: "homeCtrl"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
        
    })
    .state('contact', {
        'url': "/contact-us",
        views: {
            'topnav':{
                templateUrl: "views/modules/main-navigation.html"
            },
            'main':{
                templateUrl: "views/static/contact.html",
                controller: "contactCtrl"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('products', { 
        'url': "/products",
        views: {
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/products.html"
                /*controller:"productsCtrl"*/
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }

    })
    .state('garment',{
        'url': "/garment",
        views: {
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/garment.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('retail',{
        'url': "/retail",
        views: {
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{

                templateUrl:"views/static/retail.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('wholesaler',{
        'url': "/wholesaler",
        views: {
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/wholesaler.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('distributor',{
        'url': "/distributor",
        views: {
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/distributor.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('about',{
        'url': "/about",
        views:{
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/about.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    .state('qanda',{
        'url': "/qanda",
        views:{
            'topnav':{
                templateUrl:"views/modules/main-navigation.html"
            },
            'main':{
                templateUrl:"views/static/qanda.html"
            },
            'footer':{
                templateUrl: "views/modules/footer.html"
            }
        }
    })
    //$locationProvider.html5Mode(true).hashPrefix('!');
}]);

app.directive('wrapOwlcarousel', function () { 

    return { 
        restrict: 'E', 
        link: function (scope, element, attrs) { 
            var options = scope.$eval($(element).attr('data-options')); 
            $(element).owlCarousel(options); 
        } 
    }; 
});

app.directive('colorbox', function() {
    return {   
        restrict: 'AC',    
        link: function (scope, element, attrs) {        
        //$(element).colorbox(attrs.colorbox);     
        $(element).colorbox({closeButton: true, opacity:0.5, pagination:true, maxWidth:'100%',
            reposition:true,
            scalePhotos:true,
            scrolling:false,
            previous:'<i class="fa fa-angle-left"></i>',
            next:'<i class="fa fa-angle-left"></i>',
            close:'<i class="fa fa-times text-danger"></i>',
            current:'{current} of {total}',
            maxHeight:'100%',
            onOpen:function(){
            document.body.style.overflow = 'hidden';
            },
            onClosed:function(){
            document.body.style.overflow = 'auto';
            },
            onComplete:function(){
                $.colorbox.resize();
            }})
        }
    };  
});

app.directive('revolutionSlider', function() {
    return {   
        restrict: 'AC',    
        link: function (scope, element, attrs) {        
        console.log(element);
            $(element).revolution( {
                delay:9000,
                startwidth:1150,
                startheight:450,
                hideThumbs:10,
                
                thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                thumbHeight:50,
                thumbAmount:5,

                navigationType:"bullet",                // bullet, thumb, none
                navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none

                navigationStyle:"square",                // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom

                navigationHAlign:"center",              // Vertical Align top,center,bottom
                navigationVAlign:"bottom",              // Horizontal Align left,center,right
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,

                touchenabled:"on",                      // Enable Swipe Function : on/off
                onHoverStop:"on",                       // Stop Banner Timet at Hover on Slide on/off

                stopAtSlide:-1,
                stopAfterLoops:-1,

                hideCaptionAtLimit:0,                   // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                hideAllCaptionAtLilmit:0,               // Hide all The Captions if Width of Browser is less then this value
                hideSliderAtLimit:0,                    // Hide the whole slider, and stop also functions if Width of Browser is less than this value

                shadow:0,                               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                fullWidth:"on"               
            });
        }
    }  
});