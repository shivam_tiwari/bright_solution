var app = angular.module('static.app', []);

app.controller('homeCtrl', function($scope){

	$scope.slider = {
		sliderType: "standard",
		sliderLayout: "auto",
		responsiveLevels: [1920, 1024, 778, 480],
		gridwidth: [1930, 1240, 778, 480],
		gridheight: [768, 768, 960, 720],
		autoHeight: "off",
		minHeight: "",
		fullScreenOffsetContainer: "",
		fullScreenOffset: "",
		delay: 9000,
		disableProgressBar: "on",
		startDelay: "",
		stopAfterLoops: "",
		stopAtSlide: "",
		viewPort: {},
		lazyType: "none",
		dottedOverlay: "none",
		shadow: 0,
		spinner: "off",
		hideAllCaptionAtLilmit: 0,
		hideCaptionAtLimit: 0,
		hideSliderAtLimit: 0,
		debugMode: false,
		extensions: "",
		extensions_suffix: "",
		fallbacks: {
			simplifyAll: "off",
		  	disableFocusListener: false
		},
		parallax: {
			type: "scroll",
			origo: "enterpoint",
			speed: 400,
			levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
		},
		carousel: {},
		navigation: {
			keyboardNavigation: "off",
			keyboard_direction: "horizontal",
			mouseScrollNavigation: "off",
			onHoverStop: "on",
		  	touch: {
				touchenabled: "on",
				swipe_treshold: 75,
				swipe_min_touches: 1,
				drag_block_vertical: false,
				swipe_direction: "horizontal"
		  	},
		  	tabs: {
				style: "zeus",
				enable: true,
				width: 150,
				height: 30,
				min_width: 100,
				wrapper_padding: 0,
				wrapper_color: "transparent",
				wrapper_opacity: "0",
				tmp: "<span class='tp-tab-title'>{{title}}</span>",
				visibleAmount: 3,
				hide_onmobile: true,
				hide_under: 480,
				hide_onleave: false,
				hide_delay: 200,
				direction: "horizontal",
				span: true,
				position: "inner",
				space: 1,
				h_align: "left",
				v_align: "top",
				h_offset: 30,
				v_offset: 30
			}
		},
		jsFileLocation: "",
		visibilityLevels: [1240, 1024, 778, 480],
		hideThumbsOnMobile: "off"
	};
});

app.controller('contactCtrl', function($scope,$http){

	$scope.contact = {};
	$scope.formSubmitted = false;

	$scope.contactMe = function(){

		$scope.isLoading = true;
		$scope.isDisabled = true;

		if( $scope.contact && $scope.contact.name && $scope.contact.name.length >= 3 && 
			$scope.contact.phone && $scope.contact.message && $scope.contact.message.length >= 10 ){

			

        var request = $http({
        	method : 'POST',
        	url : 'http://brightsolution.co.in/email.php',
        	data : {
        		'name' : $scope.contact.name,
        		'email' : $scope.contact.email,
        		'phone' : $scope.contact.phone,
        		'message' : $scope.contact.message
        		}
        	})
        	request.then(function(response){

        	}, function(error){

        	});

        
			$scope.contact = {};
			$scope.formSubmitted = true;

			$scope.submitted = false;
			$scope.isLoading = false;
			$scope.isDisabled = false;
		}else{

			$scope.submitted = false;
			$scope.isLoading = false;
			$scope.isDisabled = false;
			$scope.formSubmitted = false;
			$scope.errorMessage = 'kindly fill all the fields properly.';
		}
	}
});
