<?php
header("Content-type: application/javascript");
ob_start();//compress());

    function compress( $minify )
    {
    /* remove comments */
        $minify = preg_replace( '!/*[^*]**+([^/][^*]**+)*/!', '', $minify );

        /* remove tabs, spaces, newlines, etc. */
        $minify = str_replace( array("rn", "r", "n", "t", '  ', '    ', '    '), '', $minify );

        return $minify;
    }
    /* css files for combining */
    include('../../bower_components/angular/angular.min.js');
    include('../../bower_components/angular-ui-router/release/angular-ui-router.min.js');
    include('../../bower_components/angular-ui-validate/dist/validate.min.js');
    include('angular-messages.min.js');
    include('app.js');
    include('static.app.js');
    

ob_end_flush();